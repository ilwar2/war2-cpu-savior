// cpusavior_cmd.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<stdio.h>
#include<io.h>
#include<iostream>
#include <fstream>
#include <winsock2.h>
#include<Shellapi.h>
#include <windows.h>

#define WAR2
#ifdef WAR2
#define GETTICKCOUNT_ADDR 0x00490138 // War2
#define GETPROP_ADDR 0x15033270
#define WND_NAME "Warcraft II"
#else
#define GETTICKCOUNT_ADDR 0x004FE0C4 // broodwar
#define GETPROP_ADDR 0x150452A4
#define WND_NAME "Brood War"
#endif

void PrintLastError(char *msg)
{
    LPVOID lpMsgBuf;
//    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError(); 

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );
	printf("%s: %d(%s)\n",msg, dw, lpMsgBuf);
	LocalFree(lpMsgBuf);
}


extern "C" __declspec(naked) void tickCountStub (void)
{
	__asm
	{
		__emit 0xE8	//call GetTickCount
		__emit 0xCC
		__emit 0xCC
		__emit 0xCC
		__emit 0xCC

		//cmp dword ptr [esp], 004D94B9h //1.15.2
		//cmp dword ptr [esp], 004D9519h // bw last

//		cmp dword ptr [esp], 004602C8h // war2
//		je doSleep
		cmp dword ptr [esp], 00409C32h // war2
		je doSleep
//		cmp dword ptr [esp], 00414BFCh // war2
//		je doSleep
//		cmp dword ptr [esp], 00420C33h // war2
//		je doSleep
		//cmp dword ptr [esp], 004D18CFh //1.15.2
		//cmp dword ptr [esp], 004D191Fh // bw last
//		cmp dword ptr [esp], 00409A60h // war2
		jmp noSleep

		/*__emit 0x3B	//cmp eax, [addr]
		__emit 0x05
		__emit 0xCC
		__emit 0xCC
		__emit 0xCC
		__emit 0xCC

		jne noSleep*/
doSleep:
		push eax
		push 1

		__emit 0xFF	//call IAT:Sleep
		__emit 0x15

//		__emit 0x0C	//IAT for Sleep 1.15.x
//		__emit 0xE1
//		__emit 0x4F
//		__emit 0x00
		__emit 0x68 // War2 Sleep
		__emit 0x01
		__emit 0x49
		__emit 0x00
		pop eax
noSleep:

		/*__emit 0xA3	//mov [addr], eax
		__emit 0xCC
		__emit 0xCC
		__emit 0xCC
		__emit 0xCC*/

		retn
	}
}

extern "C" __declspec (naked) void tickCountStubEnd (void)
{
	__asm int 3;
}

extern "C" __declspec(naked) void getPropStub (void)
{
	__asm
	{
		//for safety, we only sleep if we were called from storm.dll address
		//cmp dword ptr [esp], 15013E55h
//		cmp dword ptr [esp], 150135C5h
		cmp dword ptr [esp], 15011378h
		jne SkipSleep
//		jmp CheckRetAddr
//DoSleep:
		push 1
		__emit 0xFF
		__emit 0x15

//		__emit 0x0C	//IAT for Sleep 1.15.x
//		__emit 0xE1
//		__emit 0x4F
//		__emit 0x00
		__emit 0x68 // War2 Sleep
		__emit 0x01
		__emit 0x49
		__emit 0x00
SkipSleep:
		__emit 0xE9
		__emit 0xCC
		__emit 0xCC
		__emit 0xCC
		__emit 0xCC
//CheckRetAddr:
//		cmp dword ptr [esp], 15011378h
//		je DoSleep
//		jmp SkipSleep
	}
}

extern "C" __declspec (naked) void getPropStubEnd (void)
{
	__asm int 4;
}
//DWORD WINAPI DelayedPatch (VOID *arg)
DWORD WINAPI DelayedPatch (HANDLE arg)
{
	BYTE	*buff;
	DWORD	ret;
	DWORD	getTickAddr, scAddr, getPropAddr;
	HANDLE	hProcess;

//#ifndef WAR2
	Sleep (5000);
//#endif
//	hProcess = (HANDLE)arg;
	hProcess = arg;

	getTickAddr = (DWORD)GetTickCount;
	getPropAddr = (DWORD)GetPropA;
	//read current IAT entry for GetTickCount 1.15.x
	ReadProcessMemory (hProcess, (LPVOID)GETTICKCOUNT_ADDR, &scAddr, 4, &ret);
	if (ret != 4)
		return 1;

	buff = (BYTE *)VirtualAllocEx (hProcess, NULL, 256, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	
	printf("VirtualProtect size: %d\n",(DWORD)tickCountStubEnd - (DWORD)tickCountStub);

	//modify our patch stub
	VirtualProtect ((LPVOID)tickCountStub, (DWORD)tickCountStubEnd - (DWORD)tickCountStub, PAGE_READWRITE, &ret);

	//*(DWORD *)((BYTE *)tickCountStub + 4) = (long)Sleep - ((long)buff + 4 + 5);
	//*(DWORD *)((BYTE *)tickCountStub + 9) = (long)scAddr - ((long)buff + 9 + 5);

	*(DWORD *)((BYTE *)tickCountStub + 1) = (long)scAddr - ((long)buff + 1 + 5);

	//write it in
	WriteProcessMemory (hProcess, buff, tickCountStub, (DWORD)tickCountStubEnd - (DWORD)tickCountStub, &ret);
	if (ret != (DWORD)tickCountStubEnd - (DWORD)tickCountStub)
		return 1;

	//patch IAT 1.15.x
	DWORD addr = (DWORD)buff;
	VirtualProtectEx (hProcess, (LPVOID)GETTICKCOUNT_ADDR, 4, PAGE_READWRITE, &ret);
	WriteProcessMemory (hProcess, (LPVOID)GETTICKCOUNT_ADDR, &addr, 4, &ret);
	if (ret != 4)
		return 1;

	//same as before, but for GetProp
	ReadProcessMemory (hProcess, (LPVOID)GETPROP_ADDR, &scAddr, 4, &ret);
	if (ret != 4)
		return 1;

	buff = (BYTE *)VirtualAllocEx (hProcess, NULL, 256, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
	if (!buff)
		return 1;
	
	VirtualProtect ((LPVOID)getPropStub, (DWORD)getPropStubEnd - (DWORD)getPropStub, PAGE_EXECUTE_READWRITE, &ret);

	*(DWORD *)((BYTE *)getPropStub + 18) = (long)scAddr - ((long)buff + 18 + 5);

	WriteProcessMemory (hProcess, buff, getPropStub, (DWORD)getPropStubEnd - (DWORD)getPropStub, &ret);
	if (ret != (DWORD)getPropStubEnd - (DWORD)getPropStub)
		return 1;

	addr = (DWORD)buff;
	VirtualProtectEx (hProcess, (LPVOID)GETPROP_ADDR, 4, PAGE_READWRITE, &ret);
	WriteProcessMemory (hProcess, (LPVOID)GETPROP_ADDR, &addr, 4, &ret);
	if (ret != 4)
		return 1;


	FlushInstructionCache (hProcess, NULL, 0);

	return 0;
}
HANDLE war2handle;
int CheckStatus(int lat_or_TH)
{
// 0 - not started
// 1 - incompatible version
// 2 - activated
// 3 - not activated
// 4 - internal error
HWND war2wnd;
DWORD war2proc;
//HANDLE war2handle;
int buffer[]={0,0,0,0,0,0,0,0,0,0};

//war2wnd=FindWindow(NULL,"Warcraft II");
//war2wnd=FindWindow(NULL,"Brood War");
war2wnd=FindWindow(NULL,WND_NAME);
if(war2wnd==NULL)return 0;
GetWindowThreadProcessId(war2wnd,&war2proc);
if(war2proc==NULL){PrintLastError("GetWindowThreadProcessId");return 4;}
war2handle=OpenProcess(PROCESS_ALL_ACCESS,0,war2proc);
if(war2handle==NULL){PrintLastError("OpenProcess");return 4;}

return 1;
//return 4;//rand()%4;
}


int _tmain(int argc, _TCHAR* argv[])
{
	int checkstatus=0;
	printf("Waiting until the game being started\n");
	while(checkstatus==0)
	{
	checkstatus=CheckStatus(0);
//	if(!toWait)break;
	Sleep(25);
	}
	checkstatus=CheckStatus(0);
	if(checkstatus!=1)
	{
		printf("Error: checkstatus=%d,exiting\n",checkstatus);
		return 1;
	}
	printf("Applying patch\n");
	DelayedPatch(war2handle);
	printf("Patch applied\n");
	while(checkstatus!=0)
	{
	checkstatus=CheckStatus(0);
//	if(!toWait)break;
	Sleep(1000);
	}
	printf("Game finished, exiting\n");
//	char a[256];
//	gets_s(a);
	return 0;
}
