The problem: war2 always uses 100% of 1 core CPU.
This project fixes that.

Based on the same project by R1CH for StarCraft.
Thanks to xboi209 for idea and link to that project.
My research for war2 addresses in memory.

Start this console application before or during the game. It will wait until you run war2 and injects into it when it's running. Don't close it until exiting war2. It will close itself after war2 is closed.

Here's the link to original R!CH project for Starcraft BW: http://www.teamliquid.net/forum/brood-war/66104-cpu-savior-16-bwl-chaos-plugin-1161

My project contains 2 branches:
- nochat_but_stable - old one, savior works ingame only, but not in chat.
- master - the 2-nd version, additional check for bnet chat. But caused crash for me in win 8/10. Tests and fix required. 